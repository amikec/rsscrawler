from django.conf.urls import patterns, include, url
import views

urlpatterns = patterns('',
                       url(r'^feeds/load', views.LoadFeedsView.as_view(), name='feed_load'),
                       url(r'^feeds/', views.FeedListView.as_view(), name='feed_list'),
                       url(r'^feeds/active/(?P<is_active>(True|False))/$',
                           views.FeedListView.as_view(),
                           name='feed_list'),
                       url(r'^channels/(?P<pk>\d+)/articles', views.ChannelArticleView.as_view(),
                           name='channel_articles'),
                       url(r'^channels/', views.ChannelListView.as_view(), name='channel_list'),
                       url(r'^home/', views.HomeView.as_view(), name='home')
)
