from django.shortcuts import render, redirect, render_to_response
from django.core.urlresolvers import reverse
from django.views.generic import ListView, View, TemplateView
from django.template import RequestContext

from .forms import UploadFileForm
from crawler.models import Feed, Channel
from crawler.tasks import *
from crawler import monster


class FilterActiveMixin(object):
    def get_queryset(self):
        queryset = super(FilterActiveMixin, self).get_queryset()
        is_active = self.kwargs.get('is_active')

        if is_active is None:
            return queryset
        is_active = is_active == 'True'
        return queryset.filter(is_active=bool(is_active))


class HomeView(TemplateView):
    template_name = 'management/home.html'


class FeedListView(FilterActiveMixin, ListView):
    paginate_by = 50
    model = Feed
    context_object_name = 'feeds'
    template_name = 'management/feeds/list.html'


class ChannelListView(ListView):
    paginate_by = 50
    model = Channel
    context_object_name = 'channels'
    template_name = 'management/channel/list.html'


class ChannelArticleView(View):
    def get(self, request, pk):
        channel = Channel.objects.filter(pk=pk).first()
        return render(request, 'management/channel/articles.html', {
            'articles': channel.item_set.all()
        })


class LoadFeedsView(View):
    def get(self, request):
        form = UploadFileForm()
        return render_to_response('management/feeds/load.html', {'form': form}, RequestContext(request))

    def post(self, request):
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            load_feeds(request.FILES['file'])
            return redirect(reverse('management:feed_list'))

        return render_to_response('management/feeds/load.html', {'form': form}, RequestContext(request))


def load_feeds(file):

    for line in file:
        url = line.replace('\n', '').replace('\r', '')
        if Feed.objects.filter(url=url).count() > 0:
            continue

        Feed.objects.create(url=url)
