function createInputField(name, value, type) {
    // Value for type can be empty - default value for type is 'hidden'.
    type = typeof type !== 'undefined' ? type : 'hidden';
    var inputField = $(document.createElement('input'));

    inputField.attr('type', type);
    inputField.attr('name', name);
    inputField.attr('value', value);

    return inputField;
}
