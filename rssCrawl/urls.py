from django.conf.urls import patterns, include, url
from django.contrib import admin

from management.views import HomeView

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^crawler/', include('crawler.urls', namespace='crawler')),
    url(r'^management/', include('management.urls', namespace='management')),
    url(r'^$', HomeView.as_view(), name='home'),
)
