from django.db import models


class Setting(models.Model):
    is_active = models.BooleanField(default=True)
    data_path = models.CharField(max_length=250)


class Feed(models.Model):
    url = models.CharField(max_length=250)
    last_checked = models.DateTimeField(blank=True, null=True)
    is_active = models.BooleanField(default=True)
    etag = models.CharField(max_length=100)
    modified = models.CharField(max_length=100)


class RssObject(models.Model):
    title = models.CharField(max_length=250)
    link = models.CharField(max_length=250)
    description = models.TextField()

    published = models.DateTimeField(null=True, blank=True)
    author = models.CharField(max_length=50, null=True, blank=True)
    category = models.CharField(max_length=50, null=True, blank=True)
    updated = models.DateTimeField(null=True, blank=True)


class Channel(RssObject):
    feed = models.OneToOneField(Feed)

    language = models.CharField(max_length=50, null=True, blank=True)
    copyright = models.CharField(max_length=50, null=True, blank=True)
    rating = models.CharField(max_length=150, null=True, blank=True)


class Item(RssObject):
    channel = models.ForeignKey(Channel)

    item_id = models.CharField(max_length=200, null=True, blank=True, unique=True)
    path_to_article = models.CharField(max_length=250)